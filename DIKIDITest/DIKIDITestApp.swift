//
//  DIKIDITestApp.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

@main
struct DIKIDITestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
