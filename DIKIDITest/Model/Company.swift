//
//  Company.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 08.10.2022.
//

import Foundation

struct Company: Decodable {
    
    var error: CompanyError
    var data: CompanyData
    
    struct CompanyError: Decodable {
        var code: Int
        var message: String?
    }

    struct CompanyData: Identifiable, Decodable {
        var id: String
        var name: String
        var city: String
        var street: String
        var house: String
        var lat: String
        var lng: String
        var warning: String
        var rating: Int
        var description: String
        var schedule: [Schedule]
        var image: Image
        var currency: Currency
        var shares: Collection<Share>
        var groupServices: Collection<GroupService>
        var gallery: [GalleryImage]
        var services: CollectionWithStringTotal<Service>
        var masters: Collection<Master>
        var examples: Collection<Example>
        var links: [Link]
        var phones: [String]
        var reviews: CollectionWithStringTotal<Review>
        var background: Background
        
        struct Background: Decodable, Identifiable {
            var id: String
            var url: String
        }
        
        struct Review: Decodable, Identifiable {
            var id: String
            var author: String
            var rating: String
            var text: String
            var date: String
            var images: [GalleryImage]
        }
        
        struct Example: Decodable, Identifiable {
            var id: String
            var zoom: String
            var big: String
        }
        
        struct Link: Decodable {
            var code: String
            var value: String
            var link: String
            var icon: String
        }
        
        struct Master: Decodable, Identifiable {
            var id: String
            var name: String
            var post: String
            var services: String
            var image: String
        }
        
        struct Service: Decodable,  Identifiable {
            var id: String
            var name: String
            var cost: String
            var floating: String
            var time: String
            var timeFloating: String
            var icon: Icon?
            
            struct Icon: Decodable, Identifiable {
                var id: String
                var url: String
            }
        }
        
        struct GalleryImage: Decodable {
            var zoom: String
            var big: String
        }
        struct GroupService: Decodable, Identifiable {
            var id: String
            var name: String
            var image: String
            var url: String
            var cost: String
            var costFloating: Bool
            var duration: String
            var durationFloating: Bool
            var clientCount: String
            var maxCount: String
            //tags - массив Непонятно какого типа
        }
        struct Share: Decodable, Identifiable {
            var id: String
            var name: String
            var icon: String
            var timeStart: String
            var timeStop: String
            var discountValue: String
            var view: String
            var usedCount: String
        }

        
        struct Titles: Decodable {
            var service: String
            var services: String
            var resource: String
            var resources: String
            var group: String
            var groups: String
        }
        
        struct Schedule: Decodable {
            var day: String
            var workFrom: String
            var workTo: String
        }
        
        struct Image: Decodable {
            var thumb: String
            var origin: String
        }
        
        struct Currency: Decodable, Identifiable {
            var id: String
            var title: String
            var abbr: String
        }
        
    }
    
}

struct Collection<Item: Decodable>: Decodable {
    var total: Int
    var list: [Item]
}

struct CollectionWithStringTotal<Item: Decodable>: Decodable {
    var total: String
    var list: [Item]
}
