//
//  HomeView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct CompanyView: View {
    @StateObject var viewModel = CompanyViewModel()
    @State var isShowTopView = true
    @State var image: UIImage = UIImage()
    @State var showAllReviews = false
    
    let layout: [GridItem] = [GridItem(.adaptive(minimum: 200, maximum: 240), spacing: 8),
                              GridItem(.adaptive(minimum: 200, maximum: 240), spacing: 8)]
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack( alignment: .leading, spacing: 0) {
                VStack {
                    TopBarView(isShowTopView: $isShowTopView)
                        .background {
                            isShowTopView ? Color.clear : Color.white
                        }
                    HStack {
                        Image(uiImage: viewModel.uiImage)
                            .resizable()
                            .frame(width: 120, height: 120)
                            .clipShape(Circle())
                            .background(.white)
                            .padding(4)
                            .background(.white)
                            .clipShape(Circle())
                            
                        VStack(alignment: .leading) {
                            Text(viewModel.companyName)
                                .font(.title2)
                            Text(viewModel.companyAddress)
                                .font(.callout)
                            ForEach(viewModel.schedule, id: \.self) { schedule in
                                Text(schedule)
                                    .font(.callout)
                            }

                            ForEach(viewModel.phones, id: \.self) { phone in
                                Text(phone)
                                    .font(.callout)
                            }
                        }.foregroundColor(.white)
                        Spacer()
                    }
                    
                    HStack {
                        Button { } label: {
                            Text("Позвонить")
                                .foregroundColor(.white)
                                .padding(.vertical, 12)
                                .frame(maxWidth: .infinity)
                                .background(.gray.opacity(0.8))
                                .cornerRadius(6)
                                .bold()
                        }
                        
                        Button { } label: {
                            Text("Сообщение")
                                .foregroundColor(.white)
                                .padding(.vertical, 12)
                                .frame(maxWidth: .infinity)
                                .background(.gray.opacity(0.8))
                                .cornerRadius(6)
                                .bold()
                        }
                    }.padding(.horizontal, 8)
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 8)
                .background {
                    ZStack {
                        Image(uiImage: viewModel.bg)
                            .resizable()
                            .ignoresSafeArea()
                        Rectangle().opacity(0.4)
                    }
                    
                }
                
                HStack {
                    VStack {
                        Text("1886")
                            .bold()
                            .font(.title2)
                        Text("Бонусы")
                            .font(.caption)
                            .foregroundColor(.gray)
                    }
                    Spacer()
                    VStack {
                        Text("0")
                            .bold()
                            .font(.title2)
                        Text("Баланс")
                            .font(.caption)
                            .foregroundColor(.gray)
                    }
                    Spacer()
                    VStack {
                        Text("До 10%")
                            .bold()
                            .font(.title2)
                        Text("CashBack")
                            .font(.caption)
                            .foregroundColor(.gray)
                    }
                }//.frame(height: 48)
                .frame(maxWidth: .infinity)
                .padding(.horizontal, 8)
                .padding(.vertical, 6)
                .background(Color("LightGray"))
                VStack(alignment: .leading) {
                    TitleView(title: "Сертификаты")
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(viewModel.sertificates) { sertificate in
                                SertificateView(sertificate: sertificate)
                                    .environmentObject(viewModel)
                            }
                        }
                    }
                    .padding(.bottom, 8)
                }.padding(.horizontal, 8)
                
                Divider()
                
                VStack(alignment: .leading) {
                    TitleView(title: "Описание")
                    Text(viewModel.desctription)
                }.padding(.horizontal, 8)
                Divider()
                VStack(alignment: .leading) {
                    TitleView(title: "Интерьер")
                        .bold()
                        .padding(.vertical, 8)
                    ScrollView(.horizontal,
                               showsIndicators: false) {
                        HStack {
                            ForEach(viewModel.galleryImages, id: \.big) { galleryImage in
                                GalleryItem(galleryImage: galleryImage)
                                    .environmentObject(viewModel)
                            }
                        }
                    }
                }.padding(.horizontal, 8)
                VStack(alignment: .leading) {
                    TitleView(title: "Групповые услуги",
                              count: viewModel.groupServicesCount)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(viewModel.groupServices) { groupService in
                                GroupServiceView(groupService: groupService)
                                    .environmentObject(viewModel)
                            }
                        }
                    }
                }.padding(.horizontal, 8)
            }
            
            VStack(alignment: .leading) {
                TitleView(title: "Занятия", count: 120) {
                    print("Смотреть все занятия")
                }
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        ForEach(0..<6) { number in
                            ExerciseRowView()
                        }
                    }
                }
            }.padding(.horizontal, 8)
            
            VStack(alignment: .leading) {
                TitleView(title: "Инструктора", count: 12)
                ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(viewModel.insturctors) { instructor in
                                InstructorLabelView(instructor: instructor)
                                    .environmentObject(viewModel)
                            }
                    }
                }

            }.padding(.horizontal, 8)
            
            VStack(alignment: .leading) {
                TitleView(title: "Примеры работ", count: 20) {
                    print("Смотреть все примеры работ")
                }
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(viewModel.examples) { example in
                            ExampleView(example: example)
                                .environmentObject(viewModel)
                        }
                    }
                }
            }.padding(.horizontal, 8)
            
            VStack(alignment: .leading) {
                TitleView(title: "Ссылки")
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(viewModel.links, id: \.link) { link in
                            LinkView(link: link)
                                .environmentObject(viewModel)
                        }
                    }
                }
            }.padding(.horizontal, 8)
            
            VStack(alignment: .leading) {
                TitleView(title: "Отзывы") {
                    print("Смотреть все отзывы")
                    showAllReviews.toggle()
                }
                
                ReviewsView(showAll: $showAllReviews)
                    .environmentObject(viewModel)
            }.padding()

        }
        .overlay(alignment: .bottom, content: {
            Button {
                
            } label: {
                Text("Записаться")
                    .padding()
                    .background(.green)
                    .padding()
            }

        })
        .onAppear {
            viewModel.getCompanyWithAlamBy(companyID: 669)
        }
    }
    

    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        CompanyView()
    }
}
