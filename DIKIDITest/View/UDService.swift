//
//  UDService.swift
//  DIKIDITest
//
//  Created by Влад Мади on 06.10.2022.
//

import SwiftUI

class UDService {
    static let shared = UDService(); private init() { }
    
    @AppStorage("token") var token: String = ""
}
