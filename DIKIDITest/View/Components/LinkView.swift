//
//  LinkView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct LinkView: View {
    var link: Company.CompanyData.Link
    @State var image: UIImage = UIImage()
    @EnvironmentObject var companyVM: CompanyViewModel
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 40, height: 40)
            .onAppear {
                companyVM.downloadImage(by: link.icon) { result in
                    switch result {
                    case .success(let success):
                        self.image = success
                    case .failure(let failure):
                        print(failure.localizedDescription)
                    }
                }
            }
    }
}

//struct LinkView_Previews: PreviewProvider {
//    static var previews: some View {
//        LinkView()
//    }
//}
