//
//  SertificateView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct SertificateView: View {
    
    var sertificate: Company.CompanyData.Share
    @EnvironmentObject var companyVM: CompanyViewModel
    @State var image: UIImage = UIImage()
    
    var body: some View {
        ZStack(alignment: .topLeading ) {
            Image(uiImage: image)
                .resizable()
            
                .cornerRadius(12)
            VStack(alignment: .leading, spacing: 30) {
                VStack(alignment: .leading) {
                    Text(sertificate.name)
                        .font(.title2.bold())
                    Text("Описание сертификата")
                }
                Text(sertificate.discountValue)
                    .font(.title2.bold())
            }.padding(10)
                .foregroundColor(.white)
        }.frame(width: 250, height: 180)
            .onAppear {
                companyVM.downloadImage(by: sertificate.icon) { result in
                    switch result {
                    case .success(let image):
                        self.image = image
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
    }
}

//struct SertificateView_Previews: PreviewProvider {
//    static var previews: some View {
//        SertificateView(sertificate: Sh)
//    }
//}
