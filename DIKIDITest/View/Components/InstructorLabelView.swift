//
//  InstructorLabelView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct InstructorLabelView: View {
    
    var instructor: Company.CompanyData.Master
    @State var image: UIImage = UIImage()
    @EnvironmentObject var companyVM: CompanyViewModel
    
    var body: some View {
        HStack {
            Image(uiImage: image)
                .resizable()
                .frame(width: 64, height: 64)
                .clipShape(Circle())
            VStack(alignment: .leading) {
                Text(instructor.name)
                    .bold()
                Text(instructor.post)
                    .font(.caption)
                    .foregroundColor(.gray)
                Text("Занятия: \(instructor.services)")
                    .font(.caption2)
                    .foregroundColor(.gray)
            }
        }
        .onAppear {
            companyVM.downloadImage(by: instructor.image) { result in
                switch result {
                case .success(let success):
                    self.image = success
                case .failure(let failure):
                    print(failure.localizedDescription)
                }
            }
        }
    }
}

//struct InstructorLabelView_Previews: PreviewProvider {
//    static var previews: some View {
//        InstructorLabelView()
//    }
//}
