//
//  ReviewsView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct ReviewsView: View {
    
    @EnvironmentObject var companyVM: CompanyViewModel
    @Binding var showAll: Bool
    
    var body: some View {
        VStack {
            HStack {
                Text("\(companyVM.generalRating)")
                    .font(.title)
                    .fontWeight(.light)
                VStack(alignment: .leading) {
                    RatingView(rating: $companyVM.generalRating)
                    Text("\(companyVM.reviews.count) оценок")
                        .foregroundColor(.gray)
                }
                Spacer()
            }
            if showAll {
                ForEach(companyVM.reviews) { review in
                    ReviewView(review: review)
                }
            } else if !companyVM.reviews.isEmpty {
                ReviewView(review: companyVM.reviews.first!)
            }
                
            
        }
    }
    
    var generalRating: Double {
        var sum = 0.0
        for review in companyVM.reviews {
            sum += Double(review.rating)!
        }
        return sum / Double(companyVM.reviews.count)
    }
}
//
//struct ReviewsView_Previews: PreviewProvider {
//    static var previews: some View {
//        ReviewsView()
//    }
//}
