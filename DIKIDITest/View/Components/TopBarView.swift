//
//  TopBarView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct TopBarView: View {
    
    @Binding var isShowTopView: Bool
    
    var body: some View {
        HStack {
            Button { } label: {
                Image(systemName: "chevron.left")
            }
            Spacer()
            Button { } label: {
                Image(systemName: "location.circle")
            }
            Button { } label: {
                Image(systemName: "heart.fill")
            }
        }.foregroundColor(isShowTopView ? .white : .blue)
            .overlay {
                if !isShowTopView {
                    Text("Название компании")
                }
            }
            .padding(.horizontal, 8)
    }
}

//struct TopBarView_Previews: PreviewProvider {
//    static var previews: some View {
//        TopBarView(isShowTopView: )
//    }
//}
