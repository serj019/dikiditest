//
//  GalleryItem.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct GalleryItem: View {
    
    @State var image: UIImage = UIImage()
    @EnvironmentObject var companyVM: CompanyViewModel
    var galleryImage: Company.CompanyData.GalleryImage
    
    var body: some View {
            Image(uiImage: image)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 110, height: 110)
                .clipShape(RoundedRectangle(cornerRadius: 6))
                .onAppear {
                    companyVM.downloadImage(by: galleryImage.big) { result in
                        switch result {
                        case .success(let success):
                            self.image = success
                        case .failure(let failure):
                            print(failure.localizedDescription)
                        }
                    }
                }
    }
}

//struct GalleryItem_Previews: PreviewProvider {
//    static var previews: some View {
//        GalleryItem()
//    }
//}
