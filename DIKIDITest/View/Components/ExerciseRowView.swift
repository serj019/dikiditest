//
//  ExerciseRowView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct ExerciseRowView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Text("Стрижка модельная")
            HStack {
                Text("1000₽")
                Text("30 мин.")
            }
            .foregroundColor(.gray)
        }.padding(.vertical, 4)
    }
}

struct ExerciseRowView_Previews: PreviewProvider {
    static var previews: some View {
        ExerciseRowView()
    }
}
