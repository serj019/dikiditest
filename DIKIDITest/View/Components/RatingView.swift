//
//  RatingView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct RatingView: View {
    
    @Binding var rating: String
    
    var body: some View {
        HStack {
            ForEach(0..<5, id: \.self) { number in
                if Double(rating)! >= (Double(number) + 0.5) {
                    Image(systemName: "star.fill")
                } else {
                    Image(systemName: "star")
                }
            }
        }.foregroundColor(.yellow)
    }
    
}

//struct RatingView_Previews: PreviewProvider {
//    static var previews: some View {
//        RatingView()
//    }
//}
