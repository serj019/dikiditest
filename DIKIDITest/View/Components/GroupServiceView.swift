//
//  GroupServiceView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct GroupServiceView: View {
    
    var groupService: Company.CompanyData.GroupService
    @EnvironmentObject var companyVM: CompanyViewModel
    @State var image: UIImage = UIImage()
    
    var body: some View {
        VStack {
            HStack(alignment: .top ) {
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 90, height: 90)
                    .cornerRadius(12)
                Text(groupService.name)
            }
            Divider()
            HStack {
                VStack(alignment: .leading) {
                    Text("\(groupService.cost)₽")
                    Text("\(groupService.duration) ч")
                }
                Spacer()
                Button("Записаться") {}
                    .padding(8)
                    .foregroundColor(.blue)
                    .bold()
                    .background(Color("LightGray"))
                    .clipShape(Capsule())
                
            }
        }.frame(maxWidth: .infinity)
            .padding()
            .background {
                Color.white
                    .cornerRadius(12)
                    .shadow(radius: 2)
            }
            .padding(4)
            .onAppear {
                companyVM.downloadImage(by: groupService.image) { result in
                    switch result {
                    case .success(let success):
                        self.image = success
                    case .failure(let failure):
                        print(failure.localizedDescription)
                    }
                }
            }
    }
}

//struct GroupServiceView_Previews: PreviewProvider {
//    static var previews: some View {
//        GroupServiceView()
//    }
//}
