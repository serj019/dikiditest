//
//  ExampleView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct ExampleView: View {
    
    var example: Company.CompanyData.Example
    @State var image: UIImage = UIImage()
    @EnvironmentObject var companyVM: CompanyViewModel
    
    var body: some View {
        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 180, height: 180)
            .clipShape(RoundedRectangle(cornerRadius: 6))
            .onAppear {
                companyVM.downloadImage(by: example.big) { result in
                    switch result {
                    case .success(let success):
                        self.image = success
                    case .failure(let failure):
                        print(failure.localizedDescription)
                    }
                }
            }
    }
}

//struct ExampleView_Previews: PreviewProvider {
//    static var previews: some View {
//        ExampleView()
//    }
//}
