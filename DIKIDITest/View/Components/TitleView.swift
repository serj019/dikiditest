//
//  TitleView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct TitleView: View {
    
    @State var title: String = ""
    @State var count: Int? = nil
    @State var action: (() -> ())? = nil
    
    var body: some View {
        HStack {
            Text(title)
                .bold()
                .padding(.vertical, 8)
            
            if let count {
                Text("\(count)")
                    .bold()
                    .foregroundColor(.gray)
                    .padding(.vertical, 8)
            }
            Spacer()
            if let action {
                Button("См. все") { action() }
            }
            
        }
    }
}

struct TitleView_Previews: PreviewProvider {
    static var previews: some View {
        TitleView()
    }
}
