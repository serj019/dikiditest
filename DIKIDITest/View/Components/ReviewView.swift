//
//  ReviewView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 10.10.2022.
//

import SwiftUI

struct ReviewView: View {
    
    @State var review: Company.CompanyData.Review
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .frame(width: 70, height: 70)
                VStack(alignment: .leading) {
                    Text(review.author)
                        .font(.title2)
                    Text(review.date)
                        .foregroundColor(.gray)
                    RatingView(rating: $review.rating)
                }
                Spacer()
            }
            Text(review.text)
        }.padding()
            .background(Color("SuperLightGray"))
            .cornerRadius(12)
            .padding(2)
    }
}

//struct ReviewView_Previews: PreviewProvider {
//    static var previews: some View {
//        ReviewView()
//    }
//}
