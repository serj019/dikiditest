//
//  ContentView.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 06.10.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        CompanyView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
