//
//  CompanyViewModel.swift
//  DIKIDITest
//
//  Created by Serj Potapov on 08.10.2022.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class CompanyViewModel: ObservableObject {
    
    @Published var company: Company.CompanyData?
    
    var companyName: String { company?.name ?? "" }
    var companyAddress: String {
        let city = company?.city ?? ""
        let street = company?.street ?? ""
        let house = company?.house ?? ""
        let address = "\(city), \(street), кв.\(house)"
        return address
    }
    
    var desctription: String {
        guard let company else { return "" }
        return company.description
    }
    
    var image: (thumb: String, origin: String) {
        guard let company = company else { return ("", "") }
        return (company.image.thumb, company.image.origin)
    }
    
    @Published var uiImage: UIImage = UIImage()
    @Published var bg: UIImage = UIImage()
    
    var schedule: [String] {
        if let company {
            var result = [String]()
            for schedule in company.schedule {
                result.append("\(schedule.day): \(schedule.workFrom) - \(schedule.workTo)")
            }
            return result
        } else {return []}
    }
    
    var phones: [String] {
        if let company {
            return company.phones
        } else { return []}
    }
    
    var sertificates: [Company.CompanyData.Share] {
        if let company {
            return company.shares.list
        } else {
            return []
        }
    }
    
    var groupServices: [Company.CompanyData.GroupService] {
        guard let company else {
            return []
        }
        return company.groupServices.list
    }
    
    var groupServicesCount: Int {
        guard let company else {
            return 0
        }
        return company.groupServices.total
    }
    
    var insturctors: [Company.CompanyData.Master] {
        guard let company else {
            return []
        }
        return company.masters.list
    }
    
    var examples: [Company.CompanyData.Example] {
        guard let company else {
            return []
        }
        return company.examples.list
    }
    
    var links: [Company.CompanyData.Link] {
        guard let company else {
            return []
        }
        return company.links
    }
    
    @Published var reviews: [Company.CompanyData.Review] = []
    
//    {
//        guard let company else {
//            return []
//        }
//        return company.reviews.list
//    }
//
    
    func getReviews() {
        self.reviews = company!.reviews.list
    }
    @Published var generalRating: String = "0.0"
    
    func getGeneralRating() {
        var sum = 0.0
        for review in reviews {
            sum += Double(review.rating)!
        }
        self.generalRating = String(sum / Double(reviews.count))
    }
    
    var galleryImages: [Company.CompanyData.GalleryImage] {
        guard let company else {return []}
        return company.gallery
    }
    
    var urlStr = "https://api-beauty.dikidi.net/v1/company/info"
    
    func getCompanyWithAlamBy(companyID: Int) {
        
        let parameters: Parameters = ["company_id":  companyID]
        let header = HTTPHeader(name: "Authorization",
                                value: "e539cd202bf3ffac3983ecc770460788b9244c89")
        
        
        AF.request(urlStr,
                   method: .post,
                   parameters: parameters,
                   headers: [header]).validate().response { response in
            guard let data = response.data else {
                if let error = response.error {
                    print(error)
                }
                return
            }
            
            let json = JSON(data)
            print(json)
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard let company = try? decoder.decode(Company.self, from: data) else {
                print("Data failed")
                return}
            
            self.company = company.data
            self.getReviews()
            self.getGeneralRating()
            self.downloadImage(by: self.image.thumb) { result in
                switch result {
                case .success(let success):
                    self.uiImage = success
                case .failure(let failure):
                    print(failure.localizedDescription)
                }
            }
            self.downloadImage(by: company.data.background.url) { result in
                switch result {
                case .success(let success):
                    self.bg = success
                case .failure(let failure):
                    print(failure.localizedDescription)
                }
            }
        }
    }
    
    func downloadImage(by url: String, completion: @escaping (Result<UIImage, Error>) -> ()) {
        AF.download(url).responseData { response in
            guard let data = response.value else { return }
            guard let image = UIImage(data: data) else { return }
            completion(.success(image))
        }
    }
    
}


















//func getCompanyBy(companyID: Int) async throws {
//    guard let url = URL(string: urlStr) else {
//        print("invalidURL")
//        return }
//    var request = URLRequest(url: url)
//    request.httpMethod = "POST"
//    let parameters: [String: Any] = ["company_id": companyID]
//    
//    guard let httpBody = try?
//            JSONSerialization.data(withJSONObject: parameters) else {
//        print("invalidBody")
//        return }
//    request.httpBody = httpBody
//    request.addValue("e539cd202bf3ffac3983ecc770460788b9244c89", forHTTPHeaderField: "Authorization")
//    
//    let result = try await URLSession.shared.data(for: request)
//    let decoder = JSONDecoder()
//    decoder.keyDecodingStrategy = .convertFromSnakeCase
//    let company = try decoder.decode(Company.self, from: result.0)
////        print(company.data.name)
//    print(company.error.message)
//    
//}
// 
